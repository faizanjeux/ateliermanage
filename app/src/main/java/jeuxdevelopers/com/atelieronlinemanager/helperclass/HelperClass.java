package jeuxdevelopers.com.atelieronlinemanager.helperclass;

import android.content.Context;
import android.widget.Toast;

public class HelperClass {
    public static void createToast(Context context, String message) {
        Toast.makeText(context, ""+message, Toast.LENGTH_SHORT).show();
    }
}

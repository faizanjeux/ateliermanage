package jeuxdevelopers.com.atelieronlinemanager.prefrences;

import android.content.Context;
import android.content.SharedPreferences;

public class AtelierPreferences {
    private static String PREF_NAME = "AtelierPref";
    private static String KEY_FIRST_NAME = "first_name";
    private static String KEY_SUR_NAME = "sur_name";
    private static String KEY_TAX_CODE = "tax_code";
    private static String KEY_EMAIL = "email";
    private static String KEY_IMAGE = "image";
    private static String KEY_ADDRESS = "address";
    private static String KEY_USER_ID = "id";
    private static String KEY_USER_TYPE = "user_type";
    private static String KEY_IS_LOGIN = "is_login";
    private static String KEY_PSWD = "paswd";
    private static String KEY_PHONE = "phone";
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    public AtelierPreferences(Context context) {
        sharedPreferences = context.getSharedPreferences(PREF_NAME, 0);
        editor = sharedPreferences.edit();
    }

    public void setLogin(String firstName, String surName, String taxCode, String email, String image, String address, String id, String userType, boolean isLogin, String userPswd, String phone) {
        editor.putString(KEY_FIRST_NAME, firstName);
        editor.putString(KEY_SUR_NAME, surName);
        editor.putString(KEY_TAX_CODE, taxCode);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_IMAGE, image);
        editor.putString(KEY_ADDRESS, address);
        editor.putString(KEY_USER_ID, id);
        editor.putString(KEY_USER_TYPE, userType);
        editor.putString(KEY_PSWD, userPswd);
        editor.putString(KEY_PHONE, phone);
        editor.putBoolean(KEY_IS_LOGIN, isLogin);
        editor.commit();
    }

    public String getUserId() {
        return sharedPreferences.getString(KEY_USER_ID, "");
    }

    public String getName() {
        return sharedPreferences.getString(KEY_FIRST_NAME, "");
    }

    public String getEmail() {
        return sharedPreferences.getString(KEY_EMAIL, "");
    }

    public String getFirstName() {
        return sharedPreferences.getString(KEY_FIRST_NAME, "");
    }

    public String getSurName() {
        return sharedPreferences.getString(KEY_SUR_NAME, "");
    }

    public String getPhone() {
        return sharedPreferences.getString(KEY_PHONE, "");
    }

    public String getTaxCode() {
        return sharedPreferences.getString(KEY_TAX_CODE, "");
    }

    public String getImage() {
        return sharedPreferences.getString(KEY_IMAGE, "");
    }

    public String getAddress() {
        return sharedPreferences.getString(KEY_ADDRESS, "");
    }

    public String getPswd() {
        return sharedPreferences.getString(KEY_PSWD, "");
    }

    public boolean isLogin() {
        return sharedPreferences.getBoolean(KEY_IS_LOGIN, false);
    }

}

package jeuxdevelopers.com.atelieronlinemanager.model;

public class UserModel {
    private String User_First_Name, User_Sur_Name, User_Tax_Code, User_Email, User_Image, User_Address, User_UID, User_Type, User_Password, User_Phone;

    public UserModel(String user_First_Name, String user_Sur_Name, String user_Tax_Code, String user_Email, String user_Image, String user_Address, String user_UID, String user_Type, String user_Password, String user_Phone) {
        User_First_Name = user_First_Name;
        User_Sur_Name = user_Sur_Name;
        User_Tax_Code = user_Tax_Code;
        User_Email = user_Email;
        User_Image = user_Image;
        User_Address = user_Address;
        User_UID = user_UID;
        User_Type = user_Type;
        User_Password = user_Password;
        User_Phone = user_Phone;
    }

    public String getUser_First_Name() {
        return User_First_Name;
    }

    public void setUser_First_Name(String user_First_Name) {
        User_First_Name = user_First_Name;
    }

    public String getUser_Sur_Name() {
        return User_Sur_Name;
    }

    public void setUser_Sur_Name(String user_Sur_Name) {
        User_Sur_Name = user_Sur_Name;
    }

    public String getUser_Tax_Code() {
        return User_Tax_Code;
    }

    public void setUser_Tax_Code(String user_Tax_Code) {
        User_Tax_Code = user_Tax_Code;
    }

    public String getUser_Email() {
        return User_Email;
    }

    public void setUser_Email(String user_Email) {
        User_Email = user_Email;
    }

    public String getUser_Image() {
        return User_Image;
    }

    public void setUser_Image(String user_Image) {
        User_Image = user_Image;
    }

    public String getUser_Address() {
        return User_Address;
    }

    public void setUser_Address(String user_Address) {
        User_Address = user_Address;
    }

    public String getUser_UID() {
        return User_UID;
    }

    public void setUser_UID(String user_UID) {
        User_UID = user_UID;
    }

    public String getUser_Type() {
        return User_Type;
    }

    public void setUser_Type(String user_Type) {
        User_Type = user_Type;
    }

    public String getUser_Password() {
        return User_Password;
    }

    public void setUser_Password(String user_Password) {
        User_Password = user_Password;
    }

    public String getUser_Phone() {
        return User_Phone;
    }

    public void setUser_Phone(String user_Phone) {
        User_Phone = user_Phone;
    }
}

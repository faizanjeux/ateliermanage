package jeuxdevelopers.com.atelieronlinemanager.model;

public class ChatModel {
    private String message, receiver, sender, time, finish;

    public ChatModel(String message, String receiver, String sender, String time, String finish) {
        this.message = message;
        this.receiver = receiver;
        this.sender = sender;
        this.time = time;
        this.finish = finish;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getFinish() {
        return finish;
    }

    public void setFinish(String finish) {
        this.finish = finish;
    }
}
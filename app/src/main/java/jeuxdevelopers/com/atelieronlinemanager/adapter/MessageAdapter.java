package jeuxdevelopers.com.atelieronlinemanager.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.List;

import jeuxdevelopers.com.atelieronlinemanager.R;
import jeuxdevelopers.com.atelieronlinemanager.model.ChatModel;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageViewholder> {

    public static final int MSG_TYPE_LEFT = 0;
    public static final int MSG_TYPE_RIGHT = 1;
    private Context context;
    private List<ChatModel> data;
    FirebaseUser firebaseUser;
    private String currentUser;


    public MessageAdapter(Context context, List<ChatModel> data, String currentUser) {
        this.context = context;
        this.data = data;
        this.currentUser = currentUser;
    }

    @NonNull
    @Override
    public MessageViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == MSG_TYPE_RIGHT) {
            View view = LayoutInflater.from(context).inflate(R.layout.item_view_chat_right, parent, false);
            return new MessageAdapter.MessageViewholder(view);
        } else {
            View view = LayoutInflater.from(context).inflate(R.layout.item_view_chat_left, parent, false);
            return new MessageAdapter.MessageViewholder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull MessageViewholder holder, int position) {
        ChatModel chatModel = data.get(position);
        holder.txtView_Message.setText(chatModel.getMessage());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class MessageViewholder extends RecyclerView.ViewHolder {

        public TextView txtView_Message;

        public MessageViewholder(@NonNull View itemView) {
            super(itemView);
            txtView_Message = itemView.findViewById(R.id.txtView_Message);
        }
    }

    @Override
    public int getItemViewType(int position) {
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (data.get(position).getSender().equals(currentUser)) {
            return MSG_TYPE_RIGHT;
        } else {
            return MSG_TYPE_LEFT;
        }
    }
}

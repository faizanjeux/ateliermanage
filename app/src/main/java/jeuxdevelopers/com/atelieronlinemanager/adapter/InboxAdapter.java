package jeuxdevelopers.com.atelieronlinemanager.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.firebase.auth.FirebaseUser;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import jeuxdevelopers.com.atelieronlinemanager.R;
import jeuxdevelopers.com.atelieronlinemanager.activities.ChatActivity;
import jeuxdevelopers.com.atelieronlinemanager.model.UserModel;

public class InboxAdapter extends RecyclerView.Adapter<InboxAdapter.InboxViewholder> {

    private Context context;
    private List<UserModel> data;
    FirebaseUser firebaseUser;


    public InboxAdapter(Context context, List<UserModel> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public InboxViewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_view_inbox, parent, false);
        return new InboxAdapter.InboxViewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final InboxViewholder holder, int position) {
        final UserModel userModel = data.get(position);
        Log.v("ufibfweilafek", "Adapter: " + userModel.getUser_UID());
        final String name = (userModel.getUser_First_Name() + " " + userModel.getUser_Sur_Name());
        holder.txtView_Name.setText(name);
        if (!userModel.getUser_Image().equals("")) {
            Glide.with(context)
                    .load(userModel.getUser_Image())
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            holder.progressBar.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object dd, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            holder.progressBar.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .into(holder.civ_ProfilePicture);
        } else {
            Glide.with(context).load(R.drawable.ic_person).into(holder.civ_ProfilePicture);
            holder.progressBar.setVisibility(View.GONE);
        }

        holder.lnr_Item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v("ufibfweilafek", "AdapterClick: " + userModel.getUser_UID());
                context.startActivity(new Intent(context, ChatActivity.class)
                        //here the app will go to chat of specific user
                        .putExtra("USER_ID", userModel.getUser_UID())
                        .putExtra("USER_NAME", name)
                        .putExtra("USER_PICTURE", userModel.getUser_Image())
                );
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class InboxViewholder extends RecyclerView.ViewHolder {

        TextView txtView_Name;
        private CircleImageView civ_ProfilePicture;
        private ProgressBar progressBar;
        private LinearLayout lnr_Item;

        public InboxViewholder(@NonNull View itemView) {
            super(itemView);
            txtView_Name = itemView.findViewById(R.id.txtView_Name);
            civ_ProfilePicture = itemView.findViewById(R.id.civ_ProfilePicture);
            progressBar = itemView.findViewById(R.id.progressBar);
            lnr_Item = itemView.findViewById(R.id.lnr_Item);
        }
    }
}

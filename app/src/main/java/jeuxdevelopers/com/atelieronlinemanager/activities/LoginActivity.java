package jeuxdevelopers.com.atelieronlinemanager.activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import jeuxdevelopers.com.atelieronlinemanager.R;
import jeuxdevelopers.com.atelieronlinemanager.helperclass.HelperClass;
import jeuxdevelopers.com.atelieronlinemanager.prefrences.AtelierPreferences;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private static String TAG = "LoginActivity___";
    private EditText mEditTextEmail, mEditTextPassword;
    private Button mButton;
    private AtelierPreferences mPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();
        settingListener();
    }

    private void settingListener() {
        mButton.setOnClickListener(this);
    }

    private void init() {
        mEditTextEmail = findViewById(R.id.editText_Email);
        mEditTextPassword = findViewById(R.id.editText_Password);
        mButton = findViewById(R.id.button_Login);
        mPreferences = new AtelierPreferences(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (validation()) {
            loginAdmin();
        }
    }

    private void loginAdmin() {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Users").child("ServiceProvider").child("wPybIlBFHghf6OyLhMrcwnwZa9I3");
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                String email = dataSnapshot.child("user_Email").getValue().toString();
                String pswd = dataSnapshot.child("user_Password").getValue().toString();
                if (mEditTextEmail.getText().toString().equals(email) && mEditTextPassword.getText().toString().equals(pswd)) {
                    mPreferences.setLogin(
                            dataSnapshot.child("user_First_Name").getValue().toString(),
                            dataSnapshot.child("user_Sur_Name").getValue().toString(),
                            dataSnapshot.child("user_Tax_Code").getValue().toString(),
                            dataSnapshot.child("user_Email").getValue().toString(),
                            dataSnapshot.child("user_Image").getValue().toString(),
                            dataSnapshot.child("user_Address").getValue().toString(),
                            dataSnapshot.child("user_UID").getValue().toString(),
                            dataSnapshot.child("user_Type").getValue().toString(),
                            true,
                            dataSnapshot.child("user_Password").getValue().toString(),
                            dataSnapshot.child("user_Phone").getValue().toString());
                    HelperClass.createToast(LoginActivity.this, "Login Successfully");
                    navigateToActivity();
                } else {
                    HelperClass.createToast(LoginActivity.this, "Wrong email or password");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                HelperClass.createToast(LoginActivity.this, "Login Canceled");
            }
        });
    }

    private void navigateToActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private boolean validation() {
        String email = mEditTextEmail.getText().toString();
        String pass = mEditTextPassword.getText().toString();
        if (TextUtils.isEmpty(email)) {
            mEditTextEmail.setError("Please enter valid email");
            return false;
        } else if (!isEmailvalid(email)) {
            mEditTextEmail.setError("Please enter valid email");
            return false;
        } else if (TextUtils.isEmpty(pass) || pass.length() < 6) {
            mEditTextPassword.setError("Please Enter valid password");
            return false;
        } else {
            return true;
        }
    }

    private boolean isEmailvalid(CharSequence email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}

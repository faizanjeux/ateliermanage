package jeuxdevelopers.com.atelieronlinemanager.activities;

import android.accounts.Account;
import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.calendar.CalendarScopes;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventAttendee;
import com.google.api.services.calendar.model.EventDateTime;
import com.google.api.services.calendar.model.EventReminder;

import java.io.IOException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

import jeuxdevelopers.com.atelieronlinemanager.R;


public class AddGoogleEvent extends AppCompatActivity implements View.OnClickListener {
    private EditText mEditTextTitle, mEditTextDescription, mEditTextLocation;
    private TextView mEditTextStartTime, mEditTextEndTime, mEditTextStartDate, mEditTextEndDate;
    private Button mButtonCreateEvent;
    private DatePickerDialog datePickerDialog;
    private TimePickerDialog timePickerDialog;
    private Calendar startCalendar, endCalendar;
    private Button mButton;
    private static final int RC_SIGN_IN = 1122;
    private DateTime mStartDateTime, mEndDateTime;
    private static final String[] SCOPES = {CalendarScopes.CALENDAR, CalendarScopes.CALENDAR_READONLY};
    private com.google.api.services.calendar.Calendar service;
    private GoogleSignInClient mGoogleSignInClient;
    private String email;
    private static String TAG = "AddGoogleEvent___";
    private String userEmail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_google_event);
        init();
        initGoogle();
        settingListener();

    }

    private void settingListener() {
        mEditTextStartTime.setOnClickListener(this);
        mEditTextEndTime.setOnClickListener(this);
        mEditTextStartDate.setOnClickListener(this);
        mEditTextEndDate.setOnClickListener(this);
        mButton.setOnClickListener(this);
        startCalendar = Calendar.getInstance();
        endCalendar = Calendar.getInstance();
    }

    private void init() {
        mEditTextTitle = findViewById(R.id.editText_title);
        mEditTextDescription = findViewById(R.id.editText_desc);
        mEditTextLocation = findViewById(R.id.editText_location);
        mEditTextStartTime = findViewById(R.id.ediText_startTime);
        mEditTextEndTime = findViewById(R.id.ediText_endTime);
        mEditTextStartDate = findViewById(R.id.ediText_startDate);
        mEditTextEndDate = findViewById(R.id.ediText_endDate);
        mButton = findViewById(R.id.btn_addEvent);


    }

    private void initializeAccount() {
        GoogleAccountCredential mCredential = GoogleAccountCredential.usingOAuth2(
                getApplicationContext(), Arrays.asList(SCOPES))
                .setBackOff(new ExponentialBackOff());
        mCredential.setSelectedAccount(new Account(email, "jeuxdevelopers.com.atelieronlinemanager"));
        HttpTransport transport = AndroidHttp.newCompatibleTransport();
        JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
        service = new com.google.api.services.calendar.Calendar.Builder(
                transport, jsonFactory, mCredential)
                .setApplicationName("Google Calendar API Android Quickstart")
                .build();
    }


    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == mEditTextStartTime.getId()) {
            getTime("st");
        } else if (id == mEditTextEndTime.getId()) {
            getTime("et");
        } else if (id == mEditTextStartDate.getId()) {
            getDate("sd");
        } else if (id == mEditTextEndDate.getId()) {
            getDate("ed");
        } else if (id == mButton.getId()) {
            signIn();
            Log.i(TAG, "" + mStartDateTime.toString());
            Log.i(TAG, "" + mEndDateTime.toString());


        }
    }


    public void addEvent() {
        String summary = mEditTextTitle.getText().toString();
        String locatioon = mEditTextLocation.getText().toString();
        String description = mEditTextDescription.getText().toString();
        Event event = new Event()
                .setSummary(summary)
                .setLocation(locatioon)
                .setDescription(description);

        // DateTime startDateTime = new DateTime("2020-02-11T09:00:00-07:00");
        EventDateTime start = new EventDateTime()
                .setDateTime(mStartDateTime)
                .setTimeZone("America/Los_Angeles");
        event.setStart(start);


        EventDateTime end = new EventDateTime()
                .setDateTime(mEndDateTime)
                .setTimeZone("America/Los_Angeles");
        event.setEnd(end);

        String[] recurrence = new String[]{"RRULE:FREQ=DAILY;COUNT=2"};
        event.setRecurrence(Arrays.asList(recurrence));

        EventAttendee[] attendees = new EventAttendee[]{
                new EventAttendee().setEmail(userEmail)
        };
        event.setAttendees(Arrays.asList(attendees));

        EventReminder[] reminderOverrides = new EventReminder[]{
                new EventReminder().setMethod("email").setMinutes(24 * 60),
                new EventReminder().setMethod("popup").setMinutes(10),
        };
        Event.Reminders reminders = new Event.Reminders()
                .setUseDefault(false)
                .setOverrides(Arrays.asList(reminderOverrides));
        event.setReminders(reminders);

        String calendarId = "primary";

        try {
            event = service.events().insert(calendarId, event).execute();
        } catch (UserRecoverableAuthIOException e) {
            e.printStackTrace();
            startActivity(e.getIntent());
            Log.i("exceeae", "" + e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
            Log.i("exceeae", "" + e.getMessage());
        }

        Log.i("Event created", "" + event.getHtmlLink());


    }

    private void getDate(String s) {
         int startYear = 0,startMonth = 0,startDay = 0;
        if (s.equals("sd")) {


            startYear    = startCalendar.get(Calendar.YEAR); // current year
            startMonth = startCalendar.get(Calendar.MONTH); // current month
             startDay = startCalendar.get(Calendar.DAY_OF_MONTH); // current day
        } else if (s.equals("ed")) {
             startYear = endCalendar.get(Calendar.YEAR); // current year
             startMonth = endCalendar.get(Calendar.MONTH); // current month
             startDay = endCalendar.get(Calendar.DAY_OF_MONTH); // current day
        }


// date picker dialog
        datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        // set day of month , month and year value in the edit text

                        if (s.equals("sd")) {
                            startCalendar.set(Calendar.YEAR, year);
                            startCalendar.set(Calendar.MONTH, monthOfYear);
                            startCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                            mEditTextStartDate.setText(dayOfMonth + "/"
                                    + (monthOfYear + 1) + "/" + year);
                            Date date = startCalendar.getTime();
                            mStartDateTime = new DateTime(date);
                        } else if (s.equals("ed")) {

                            endCalendar.set(Calendar.YEAR, year);
                            endCalendar.set(Calendar.MONTH, monthOfYear);
                            endCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                            mEditTextEndDate.setText(dayOfMonth + "/"
                                    + (monthOfYear + 1) + "/" + year);
                            Date date = endCalendar.getTime();
                            mEndDateTime = new DateTime(date);
                        }

                    }
                }, startYear, startMonth, startDay);
        datePickerDialog.show();
    }


    public void getTime(String s) {
        final int startHour = startCalendar.get(Calendar.HOUR_OF_DAY);
        final int startMinute = startCalendar.get(Calendar.MINUTE);
        timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                if (s.equals("st")) {
                    startCalendar.set(Calendar.HOUR, startHour);
                    startCalendar.set(Calendar.MINUTE, startMinute);
                    mEditTextStartTime.setText(selectedHour + ":" + selectedMinute);
                    Date date = startCalendar.getTime();
                    mStartDateTime = new DateTime(date);
                } else if (s.equals("et")) {
                    endCalendar.set(Calendar.HOUR, startHour);
                    endCalendar.set(Calendar.MINUTE, startMinute);
                    mEditTextEndTime.setText(selectedHour + ":" + selectedMinute);
                    Date date = endCalendar.getTime();
                    mEndDateTime = new DateTime(date);
                }
            }
        }, startHour, startMinute, true);//Yes 24 hour time
        timePickerDialog.setTitle("Select Time");
        timePickerDialog.show();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);

        }
    }

    @SuppressLint("NewApi")
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            //  updateUI(account);

            Log.i("afaf", "" + account.getEmail());
            userEmail = account.getEmail();
            email = Objects.requireNonNull(account.getAccount()).name;
            initializeAccount();
            Thread thread = new Thread(() -> addEvent());
            thread.start();

        } catch (Exception e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.i("Google Exception", "signInResult:failed code=" + e.getMessage());
            //updateUI(null);
        }
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void initGoogle() {
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
    }
}

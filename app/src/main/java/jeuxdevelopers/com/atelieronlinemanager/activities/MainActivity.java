package jeuxdevelopers.com.atelieronlinemanager.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import jeuxdevelopers.com.atelieronlinemanager.R;
import jeuxdevelopers.com.atelieronlinemanager.adapter.InboxAdapter;
import jeuxdevelopers.com.atelieronlinemanager.model.UserModel;

public class MainActivity extends AppCompatActivity {
    private static String TAG = "MainActivity___";
    private List<String> userIds;
    private ArrayList<UserModel> allUsers;
    private ImageView img_NoData;
    private RecyclerView recView_chatUsers;
    private InboxAdapter inboxAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        img_NoData = findViewById(R.id.img_NoData);
        recView_chatUsers = findViewById(R.id.recView_chatUsers);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recView_chatUsers.setLayoutManager(linearLayoutManager);
        getUserIds();
    }

    private void getUserIds() {
        userIds = new ArrayList<>();
        userIds.clear();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Chat");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChildren()) {
                    for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                        userIds.add(snapshot.getKey());
                    }
                    Log.v("asdfghjkl", "UserIds: " + userIds.size());
                    if (userIds.size() > 0) {
                        img_NoData.setVisibility(View.GONE);
                        getUserData();
                    } else {
                        img_NoData.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void getUserData() {
        allUsers = new ArrayList<>();
        allUsers.clear();
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Users").child("Client");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    if ((userIds.contains(snapshot.child("user_UID").getValue().toString()))) {
                        allUsers.add(new UserModel(
                                snapshot.child("user_First_Name").getValue().toString(),
                                snapshot.child("user_Sur_Name").getValue().toString(),
                                snapshot.child("user_Tax_Code").getValue().toString(),
                                snapshot.child("user_Email").getValue().toString(),
                                snapshot.child("user_Image").getValue().toString(),
                                snapshot.child("user_Address").getValue().toString(),
                                snapshot.child("user_UID").getValue().toString(),
                                snapshot.child("user_Type").getValue().toString(),
                                snapshot.child("user_Password").getValue().toString(),
                                snapshot.child("user_Phone").getValue().toString()
                        ));
                    }
                }

                inboxAdapter = new InboxAdapter(MainActivity.this, allUsers);
                recView_chatUsers.setAdapter(inboxAdapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}

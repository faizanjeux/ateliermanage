package jeuxdevelopers.com.atelieronlinemanager.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.appcompat.app.AppCompatActivity;

import jeuxdevelopers.com.atelieronlinemanager.R;
import jeuxdevelopers.com.atelieronlinemanager.prefrences.AtelierPreferences;

public class SplashActivity extends AppCompatActivity {

    private AtelierPreferences atelierPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        atelierPreferences = new AtelierPreferences(SplashActivity.this);
        Animation move_and_fade = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.move_and_fade);
        findViewById(R.id.img_Icon).startAnimation(move_and_fade);
        final Handler handler = new Handler();
        handler.postDelayed(() -> {
            if (atelierPreferences.isLogin()) {
                startActivity(new Intent(this, MainActivity.class));
                finish();
            } else {
                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                finish();
            }
        }, 3000);
    }
}

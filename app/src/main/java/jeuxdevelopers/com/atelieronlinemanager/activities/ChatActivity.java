package jeuxdevelopers.com.atelieronlinemanager.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import jeuxdevelopers.com.atelieronlinemanager.R;
import jeuxdevelopers.com.atelieronlinemanager.adapter.MessageAdapter;
import jeuxdevelopers.com.atelieronlinemanager.model.ChatModel;

public class ChatActivity extends AppCompatActivity {

    private EditText edtTxt_Message;
    private FloatingActionButton fab_Send;
    private String currentUser;
    private String receiverId, receiverName, receiverPicture;
    private CircleImageView civ_ProfilePicture;
    private TextView txtView_Name;
    private Context context;
    private RecyclerView rclView_Chat;
    List<ChatModel> chatData;
    DatabaseReference reference;
    MessageAdapter messageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        context = ChatActivity.this;
        getIntentData();
        linkViews();
        setData();
        currentUser = getString(R.string.atelier_uid);

        rclView_Chat.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setStackFromEnd(true);
        rclView_Chat.setLayoutManager(linearLayoutManager);

        reference = FirebaseDatabase.getInstance().getReference("Users").child("Client").child(receiverId);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                readMessage(currentUser, receiverId);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        findViewById(R.id.fab_Send).setOnClickListener(v -> {
            String message = edtTxt_Message.getText().toString();
            if (!TextUtils.isEmpty(message)) {
                sendMessage(currentUser, receiverId, message, "");
            } else {
                Toast.makeText(ChatActivity.this, "You can't send empty message", Toast.LENGTH_SHORT).show();
            }
            edtTxt_Message.setText("");
        });

        findViewById(R.id.img_EndChat).setOnClickListener(v -> {
            setFinishRef();
        });
    }

    private void setData() {
        txtView_Name.setText(receiverName);
        if (!receiverPicture.equals("")) {
            Glide.with(context)
                    .load(receiverPicture)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object dd, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    })
                    .into(civ_ProfilePicture);
        } else {
            Glide.with(context).load(R.drawable.ic_person).into(civ_ProfilePicture);
        }
    }

    private void getIntentData() {
        Intent intent = getIntent();
        receiverId = intent.getStringExtra("USER_ID");
        Log.v("ufibfweilafek", "getIntent: " + receiverId);
        receiverName = intent.getStringExtra("USER_NAME");
        receiverPicture = intent.getStringExtra("USER_PICTURE");
    }

    private void linkViews() {
        civ_ProfilePicture = findViewById(R.id.civ_ProfilePicture);
        txtView_Name = findViewById(R.id.txtView_Name);
        edtTxt_Message = findViewById(R.id.edtTxt_Message);
        rclView_Chat = findViewById(R.id.rclView_Chat);
    }

    private void sendMessage(String sender, String receiver, String message, String finish) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child("Chat").child(receiverId);
        HashMap data = new HashMap();
        data.put("Sender", currentUser);
        data.put("Receiver", receiverId);
        data.put("Message", message);
        data.put("Time", String.valueOf(System.currentTimeMillis()));
        data.put("Finish", finish);
        reference.push().setValue(data);
    }

    private void readMessage(String receiver, String sender) {
        Log.v("ufibfweilafek", "Receiver: " + receiverId);
        chatData = new ArrayList<>();
        reference = FirebaseDatabase.getInstance().getReference("Chat").child(receiverId);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                chatData.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    String Message = snapshot.child("Message").getValue().toString();
                    String Receiver = snapshot.child("Receiver").getValue().toString();
                    String Sender = snapshot.child("Sender").getValue().toString();
                    String Time = snapshot.child("Time").getValue().toString();
                    String Finish = snapshot.child("Finish").getValue().toString();
                    chatData.add(new ChatModel(Message, Receiver, Sender, Time, Finish));
                }

                Log.v("ufibfweilafek", "ChatSize: " + chatData.size());

                messageAdapter = new MessageAdapter(ChatActivity.this, chatData, currentUser);
                rclView_Chat.setAdapter(messageAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void backButtonMyData(View view) {
        onBackPressed();
    }

    private void setFinishRef() {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Finish").child(receiverId);
        reference.setValue("Yes");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue().toString().equalsIgnoreCase("YesClicked")) {
                    Toast.makeText(context, "User ended the chat", Toast.LENGTH_SHORT).show();
                    reference.setValue("No");
                    Intent intent = new Intent(ChatActivity.this, AddGoogleEvent.class);
                    startActivity(intent);

                } else if (dataSnapshot.getValue().toString().equalsIgnoreCase("NoClicked")) {
                    Toast.makeText(context, "User Do not want to end chat", Toast.LENGTH_SHORT).show();
                    reference.setValue("No");
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
